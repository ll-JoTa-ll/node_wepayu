/**
 * BancoController
 *
 * @description :: Server-side logic for managing Bancoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */


module.exports = {
	
listar: function(req, res){
  Banco
   .find()
   //.sort({Serie: "DESC"})
   .then(function(registros){
    res.json({registros: registros});
   })
   .catch(function(err){
    res.negotiate(err);
   });
 }

};
