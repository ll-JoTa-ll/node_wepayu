/**
 * SolicitudController
 *
 * @description :: Server-side logic for managing Solicituds
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

listar: function(req, res){
  Solicitud
   .find()
   //.sort({Serie: "DESC"})
   .then(function(registros){
    res.json({registros: registros});
   })
   .catch(function(err){
    res.negotiate(err);
   });
},

listarFiltro: function(req, res){

	var data = req.allParams();

	  Solicitud
	   .find(
	   		{
	   			id_estado: data.id_estado
	   		}
	   	)
	   //.sort({Serie: "DESC"})
	   .then(function(registros){
	    res.json({registros: registros});
	   })
	   .catch(function(err){
	    res.negotiate(err);
	   });
},

insertar: function(req, res){
		var data = req.allParams();
		Solicitud
			.create(data)
			.then(function(registro){
				//res.ok();
				res.json({registro: registro});
			})
			.catch(function(err){
				res.negotiate(err);
			});
},

actualizar: function (req, res) {
	    var data = req.allParams();
	    var filtro = {id_solicitud: data.id_solicitud};

	    Solicitud
	      .update(filtro, data)
	      .then(function (registro) {
	        res.json({registro: registro});
	      })
	      .catch(function (err) {
	        res.negotiate(err);
	      });
}
	
};

