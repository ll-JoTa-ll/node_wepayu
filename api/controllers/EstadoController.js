/**
 * EstadoController
 *
 * @description :: Server-side logic for managing estadoes
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

  listar: function (req, res) {
    Estado
      .find()
      //.sort({Serie: "DESC"})
      .then(function (registros) {
        res.json({ registros: registros });
      })
      .catch(function (err) {
        res.negotiate(err);
      });
  }

};

