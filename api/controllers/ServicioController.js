/**
 * ServicioController
 *
 * @description :: Server-side logic for managing Servicios
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

      listar: function (req, res) {

            Servicio
                  .find()
                  //.sort({Serie: "DESC"})
                  .then(function (registros) {
                        res.json({ registros: registros });
                  })
                  .catch(function (err) {
                        res.negotiate(err);
                  });
      }
      
};

