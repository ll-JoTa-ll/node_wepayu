/**
 * Solicitud.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  tableName: "t_solicitud",
  attributes: {
    id_solicitud: {
                  primaryKey: true,
                  unique: true,
                  autoIncrement: true,
                  type: "integer",
                },
   id_usuario_alex: "integer",
   id_usuario_bryan: "integer",
   id_servicio: "integer",
   monto_pagar_bryan: "float",
   monto_depositar_alex: "float",
   descuento: "float",
   id_estado: "integer",
   voucher_alex: "string",
   voucher_bryan: "string",
   voucher_wepayu: "string",
   cod_servicio_alex: "string",
   descripcion: "string"
  },
  autoCreatedAt: false,
  autoUpdatedAt: false
};

