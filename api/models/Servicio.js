/**
 * Servicio.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {

  tableName: "t_servicios",
  attributes: {
    id_servicio: {
      primaryKey: true,
      unique: true,
      autoIncrement: true,
      type: "integer",
    }
    , estado: "integer"
    , nombre: "string"
  },
  autoCreatedAt: false,
  autoUpdatedAt: false

};

