	/**
 * Banco.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */


module.exports = {
  tableName: "t_bancos",
  attributes: {
    id_banco: {
                  primaryKey: true,
                  unique: true,
                  autoIncrement: true,
                  type: "integer",
                },
   nombre: "string",
   url_banco: "string",
   logo: "string"
  },
  autoCreatedAt: false,
  autoUpdatedAt: false
};


